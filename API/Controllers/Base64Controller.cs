﻿// <copyright file="Base64Controller.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace API.Controllers
{
	using App;
	using Microsoft.AspNetCore.Mvc;

	/// <summary>
	/// Main controller of the encrypt and decrypt operations.
	/// </summary>
	[ApiController]
	[Route("api/v1/encryption/[controller]")]
	public class Base64Controller : Controller
	{
		private readonly AppImplementation _app = new AppImplementation();

		/// <summary>
		/// Encrypt a string to base64.
		/// </summary>
		/// <param name="message">String to encrypt.</param>
		/// <returns>Encyprted string.</returns>
		[Route("encrypt/{message}")]
		[HttpGet]
		public IActionResult Encrypt(string message)
		{
			string base64String = this._app.EncryptBase64(message);
			return this.Ok(base64String);
		}

		/// <summary>
		/// Decrypts a base64 string to normal text.
		/// </summary>
		/// <param name="encodeMessage">String to decrypt.</param>
		/// <returns>Decrypted string.</returns>
		[Route("decrypt/{encodeMessage}")]
		[HttpGet]
		public IActionResult Decrypt(string encodeMessage)
		{
			string decodeMessage = this._app.DecryptBase64(encodeMessage);
			return this.Ok(decodeMessage);
		}
	}
}
