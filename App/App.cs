﻿// <copyright file="App.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace App;

using System;

/// <summary>
/// Class that handles encryption and decryption of base64 files.
/// </summary>
public class AppImplementation
{
	/// <summary>
	/// Encrypts a string with base64 notation.
	/// </summary>
	/// <param name="encryptable">String to encrypt.</param>
	/// <returns>Encyprted string.</returns>
	public string EncryptBase64(string encryptable)
	{
		var bytesToEncode = System.Text.Encoding.UTF8.GetBytes(encryptable);

		return Convert.ToBase64String(bytesToEncode);
	}

	/// <summary>
	/// Decrypts a string with base64 notation.
	/// </summary>
	/// <param name="decryptable">String to decrypt.</param>
	/// <returns>Decrypted string.</returns>
	public string DecryptBase64(string decryptable)
	{
		var toDecrypt = Convert.FromBase64String(decryptable);
		return System.Text.Encoding.UTF8.GetString(toDecrypt);
	}
}
