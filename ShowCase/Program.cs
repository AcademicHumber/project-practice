﻿// <copyright file="Program.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace ShowCase;
using App;
using System.CommandLine;
using System.CommandLine.NamingConventionBinder;
using System.Reflection;
using System.Threading.Tasks;

/// <summary>
/// Main program of the project.
/// </summary>
public static class Program
{
	/// <summary>
	/// Main program.
	/// </summary>
	/// <param name="args">Args to initialize the program.</param>
	/// <returns>Command Line handler.</returns>
	public static async Task<int> Main(string[] args)
	{
		var cmd = new RootCommand
			{
				new Command("encrypt", "Encrypt a string to base64.")
				{
					new Argument<string>("toBase64String", "The string to encrypt to base 64."),
				}.WithHandler(nameof(WriteBase64)),

				new Command("decrypt", "Decrypt a string from base64.")
				{
					new Argument<string>("base64String", "The string to decrypt from base 64."),
				}.WithHandler(nameof(WriteString)),
			};

		return await cmd.InvokeAsync(args);
	}

	/// <summary>
	/// Receive the function to handle the command.
	/// </summary>
	/// <param name="command">The command to utilize.</param>
	/// <param name="methodName">The method that will be executed with the command.</param>
	/// <returns>Method callback.</returns>
	private static Command WithHandler(this Command command, string methodName)
	{
		var method = typeof(Program).GetMethod(methodName, BindingFlags.NonPublic | BindingFlags.Static);
		var handler = CommandHandler.Create(method!);
		command.Handler = handler;
		return command;
	}

	/// <summary>
	/// Receive the function to handle the command.
	/// </summary>
	/// <param name="toBase64">The string to encrypt and print to console.</param>
	private static void WriteBase64(string toBase64)
	{
		AppImplementation app = new AppImplementation();
		Console.WriteLine(app.EncryptBase64(toBase64));
	}

	/// <summary>
	/// Receive the function to handle the command.
	/// </summary>
	/// <param name="fromBase64">The string to encrypt and print to console.</param>
	private static void WriteString(string fromBase64)
	{
		AppImplementation app = new AppImplementation();
		Console.WriteLine(app.DecryptBase64(fromBase64));
	}
}