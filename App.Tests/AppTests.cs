// <copyright file="AppTests.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace App.Tests;

using Microsoft.VisualStudio.TestTools.UnitTesting;

/// <summary>
/// Class that handles encryption and decryption of base64 files.
/// </summary>
[TestClass]
public class AppTests
{
	private readonly AppImplementation _app;

	/// <summary>
	/// Initializes a new instance of the <see cref="AppTests"/> class.
	/// </summary>
	public AppTests()
	{
		this._app = new AppImplementation();
	}

	/// <summary>
	/// Tests EncryptBase64 function.
	/// </summary>
	[TestMethod]
	public void ItEncrypts()
	{
		string encrypted = this._app.EncryptBase64("Hi, im coding! :D");

		Assert.AreEqual(encrypted, "SGksIGltIGNvZGluZyEgOkQ=");
	}

	/// <summary>
	/// Tests DecryptBase64 function.
	/// </summary>
	[TestMethod]
	public void ItDecrypts()
	{
		string decrypted = this._app.DecryptBase64("SGksIGltIGNvZGluZyEgOkQ=");

		Assert.AreEqual(decrypted, "Hi, im coding! :D");
	}
}