FROM mcr.microsoft.com/dotnet/sdk:6.0
COPY release/app/ App/
WORKDIR /App
ENV ASPNETCORE_URLS http://*:5000
ENV ASPNETCORE_ENVIRONMENT=Development
ENTRYPOINT ["dotnet", "API.dll"]